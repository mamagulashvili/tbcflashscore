package com.example.tbcfriday7.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.tbcfriday7.ParentAdapter
import com.example.tbcfriday7.R
import com.example.tbcfriday7.databinding.MatchFragmentBinding
import com.example.tbcfriday7.models.Match
import com.example.tbcfriday7.models.Summary

class MatchFragment : Fragment() {
    private val viewModel: MatchViewModel by viewModels()
    private var _binding: MatchFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var recAdapter: ParentAdapter
    private val firstList = mutableListOf<Summary>()
    private val secondList = mutableListOf<Summary>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = MatchFragmentBinding.inflate(layoutInflater, container, false)
        init()
        return binding.root
    }

    private fun init() {
        viewModel.getMatch()
        observe()
    }

    private fun observe() {
        viewModel.match.observe(viewLifecycleOwner, {
            it.body()?.let { match -> setHeaderData(match.match) }
            val summaryList = it.body()?.match?.matchSummary?.summaries
            setRecycle(summaryList as MutableList<Summary>)
        })
    }

    private fun setHeaderData(
        match: Match
    ) {
        Glide.with(binding.ivFirstTeam.context).load(match.team1?.teamImage)
            .placeholder(R.drawable.ic_football_club).into(binding.ivFirstTeam)
        Glide.with(binding.ivSecondTeam.context).load(match.team2?.teamImage)
            .placeholder(R.mipmap.ic_launcher).into(binding.ivSecondTeam)
        binding.apply {
            tvDateAndStadium.text = "${match.matchDate}   ${match.stadiumAdress}"
            tvFirstTeamPossession.text = match.team1?.ballPosition.toString()
            tvSecondTeamPossession.text = "${match.team2?.ballPosition.toString()}%"
            tvMatchTime.text = match.matchTime.toString()
            tvFirstTeamScore.text = match.team1?.score.toString()
            tvSecondTeamScore.text = match.team2?.score.toString()
            tvFirstTeamName.text = match.team1?.teamName
            tvSecondTeamName.text = match.team2?.teamName
            possessionProgressBar.progress = match.team1?.ballPosition!!
        }
    }

    private fun setRecycle(
        summaryList: MutableList<Summary>,
    ) {
        recAdapter = ParentAdapter(summaryList)
        binding.rvSummary.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = recAdapter
            isNestedScrollingEnabled
        }
    }

}