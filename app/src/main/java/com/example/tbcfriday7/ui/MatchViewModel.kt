package com.example.tbcfriday7.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.tbcfriday7.api.RetrofitService
import com.example.tbcfriday7.models.MatchResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class MatchViewModel : ViewModel() {
    val match: MutableLiveData<Response<MatchResponse>> = MutableLiveData()

    fun getMatch() = viewModelScope.launch(Dispatchers.IO) {
        val response = RetrofitService.api.getMatch()
        match.postValue(response)
    }
}