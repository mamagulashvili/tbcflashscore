package com.example.tbcfriday7.models

data class MatchSummary(
    val summaries: List<Summary>
)