package com.example.tbcfriday7.models

data class Team2(
    val ballPosition: Int?,
    val score: Int?,
    val teamImage: String?,
    val teamName: String?
)