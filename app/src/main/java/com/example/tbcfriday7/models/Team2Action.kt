package com.example.tbcfriday7.models

data class Team2Action(
    val action: Action?,
    val actionType: Int?,
    val teamType: Int?
)