package com.example.tbcfriday7.models

data class MatchResponse(
    val match: Match,
    val resultCode: Int?
)