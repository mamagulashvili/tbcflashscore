package com.example.tbcfriday7.models

data class Player(
    val playerImage: String?,
    val playerName: String?
)