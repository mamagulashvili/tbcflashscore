package com.example.tbcfriday7

enum class ActionType(val action:Int){
    GOAL(1),
    YELLOW_CARD(2),
    RED_CARD(3),
    SUBSTITUTION(4),
}

enum class GoalType(val type:Int){
    GOAL(1),
    OWN_GOAL(2)
}