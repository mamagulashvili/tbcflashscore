package com.example.tbcfriday7.api

import com.example.tbcfriday7.models.MatchResponse
import retrofit2.Response
import retrofit2.http.GET

interface MatchApi {
    @GET("/v3/48bb936e-261a-4471-a362-3bbb3b9a2a58")
    suspend fun getMatch(): Response<MatchResponse>
}