package com.example.tbcfriday7

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.example.tbcfriday7.databinding.ChildSecondItemBinding
import com.example.tbcfriday7.models.*

class SecondTeamActionsAdapter(
    val team2Action: MutableList<Team2Action>,
) : RecyclerView.Adapter<SecondTeamActionsAdapter.ChildViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChildViewHolder {
        return ChildViewHolder(
            ChildSecondItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ChildViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount(): Int {
        return team2Action.size
    }

    inner class ChildViewHolder(val binding: ChildSecondItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun onBind() {
            val players = team2Action[adapterPosition].action

            when (team2Action[adapterPosition].actionType) {
                ActionType.GOAL.action -> {
                    when (team2Action[adapterPosition].action?.goalType) {
                        GoalType.GOAL.type-> {
                            binding.apply {
                                ivActionImage.setImageResource(R.drawable.ic_goal)
                                tvPlayerName.text  = players?.player?.playerName
                                ivPlayerImage.setImage(players?.player?.playerImage!!)
                            }
                        }
                        GoalType.OWN_GOAL.type -> {
                            binding.apply {
                                ivActionImage.setImageResource(R.drawable.ic_own_goal)
                                tvPlayerName.text  = players?.player?.playerName
                                ivPlayerImage.setImage(players?.player?.playerImage)
                            }
                        }
                    }
                }
                ActionType.YELLOW_CARD.action -> {
                    binding.apply {
                        ivActionImage.setImageResource(R.drawable.icyellow_card)
                        tvPlayerName.text= players?.player?.playerName
                        ivPlayerImage.setImage(players?.player?.playerImage)
                    }
                }
                ActionType.RED_CARD.action -> {
                    binding.apply {
                        ivActionImage.setImageResource(R.drawable.ic_red_card)
                        tvPlayerName.text= players?.player?.playerName
                        ivPlayerImage.setImage(players?.player?.playerImage!!)
                    }
                }
                ActionType.SUBSTITUTION.action -> {
                    binding.apply {
                        ivActionImage.setImageResource(R.drawable.ic_substitution)
                        ivSecondPlayer.isVisible = true
                        tvSecondPlayerName.isVisible = true
                        tvPlayerName.text = players?.player1?.playerName
                        tvSecondPlayerName.text = players?.player2?.playerName
                        ivPlayerImage.setImage(players?.player1?.playerImage)
                        ivSecondPlayer.setImage(players?.player2?.playerImage)
                    }

                }
            }


        }
    }

}