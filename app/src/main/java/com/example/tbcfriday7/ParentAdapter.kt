package com.example.tbcfriday7

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.tbcfriday7.databinding.RowSummaryItemBinding
import com.example.tbcfriday7.models.Summary
import com.example.tbcfriday7.models.Team1Action
import com.example.tbcfriday7.models.Team2Action

class ParentAdapter(
    val summaryList: MutableList<Summary>,
) : RecyclerView.Adapter<ParentAdapter.ParentViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentViewHolder {
        return ParentViewHolder(
            RowSummaryItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ParentViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount(): Int = summaryList.size

    inner class ParentViewHolder(val binding: RowSummaryItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            binding.tvTime.text = summaryList[adapterPosition].actionTime

            val team1ActionList = mutableListOf<Team1Action>()
            val team2ActionList = mutableListOf<Team2Action>()

            if (summaryList[adapterPosition].team1Action != null) {
                summaryList[adapterPosition].team1Action?.let { team1ActionList.addAll(it) }
            }
            if (summaryList[adapterPosition].team2Action != null) {
                summaryList[adapterPosition].team2Action?.let { team2ActionList.addAll(it) }
            }
            binding.rvChildFirst.apply {
                layoutManager = LinearLayoutManager(binding.rvChildFirst.context)
                adapter = FirstTeamActionsAdapter(team1ActionList)
            }
            binding.rvChildSecond.apply {
                layoutManager = LinearLayoutManager(binding.rvChildSecond.context)
                adapter = SecondTeamActionsAdapter(team2ActionList)
            }
        }
    }
}