package com.example.tbcfriday7

import androidx.appcompat.widget.AppCompatImageView
import com.bumptech.glide.Glide

fun AppCompatImageView.setImage(imageUrl: String?) {
    Glide.with(this.context).load(imageUrl).placeholder(R.drawable.ic_football_player).into(this)
}